import pygame
import datetime
import sprite
import ship
import astroids
import collision

class game:
    def run():
        pygame.init()
        pygame.font.init()
        clock = pygame.time.Clock()
        game_running = True
        round_running = True
        game_over = False

        screen_wh = (1200, 800)

        display = pygame.display.set_mode(screen_wh)
        pygame.display.set_caption("Dodge The Astroids!")
        collisions = collision.collide()
        
        while game_running:
            space_ship = ship.ship(sprite.sprite(pygame.image.load("assets/space_ship2.png"), 550, 700, 80, 80))
            astroidsN = astroids.astroids(10)
            score = 0

            #run game
            while round_running:
                #get user input
                for event in pygame.event.get():
                    space_ship.x_change(event)

                    if event.type == pygame.QUIT:
                        round_running = False
                        game_running = False

                #detect space ship and astroid collisions
                collided = collisions.collided(astroidsN, space_ship)

                if collided:
                    round_running = False
                    game_over = True

                #change sprite coords
                space_ship.move(screen_wh[0])
                astroidsN.move()

                #draw
                display.fill((0, 0, 0x66))
                space_ship.draw(display)
                astroidsN.draw(display)
                display.blit(pygame.font.SysFont("Comic Sans MS", 30).render("Score: " + str(score), 0, (255, 255, 255)), (550, 0))

                #display
                pygame.display.update()
                clock.tick(60)

                #increment score
                score += 1
    
            #run game over screen
            while game_over:
                #get user input
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        game_over = False
                        game_running = False
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_UP:
                            game_over = False
                            round_running = True
            
                # draw game over text
                display.blit(pygame.font.SysFont("Comic Sans MS", 100).render("GAME OVER!", 0, (255, 255, 255)), (390, 200))
                display.blit(pygame.font.SysFont("Comic Sans MS", 30).render("Press UP To Continue!", 0, (255, 255, 255)), (500, 300))

                #display
                pygame.display.update()
                clock.tick(60)

        #quit
        pygame.quit()
        return True
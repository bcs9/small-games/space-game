import random
import sprite

class astroid:
    def __init__(self, sprite):
        self.sprite = sprite
        self.speed = random.randint(5, 15)

    def move(self):
        self.sprite.cy = self.speed
        self.sprite.move()

        if self.sprite.y > 850:
            self.sprite.x = random.randint(0, 1200)
            self.sprite.y = -100
    
    def draw(self, display):
        self.sprite.draw(display)
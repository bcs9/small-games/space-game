import sprite
import pygame

class ship:
    def __init__(self, sprite):
        self.sprite = sprite
    
    def x_change(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_a or event.key == pygame.K_LEFT:
                self.sprite.cx = -10
            if event.key == pygame.K_d or event.key == pygame.K_RIGHT:
                self.sprite.cx = 10
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_a or event.key == pygame.K_LEFT:
                self.sprite.cx = 0
            if event.key == pygame.K_d or event.key == pygame.K_RIGHT:
                self.sprite.cx = 0
        
    
    def move(self, w_window):
        self.sprite.move()

        self.sprite.x %= w_window

    def draw(self, display):
        self.sprite.draw(display)
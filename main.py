import game

def main():
    g = game.game.run()

    if g == False:
        print("The program quit because of an error!")
    
    quit()

main()
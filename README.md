# Dodge The Astroids!

A simple game of dodging astroids.

![](./screenshots/game-over.png)

## Controls

Left Arrow: Go Left

Right Arrow: Go Right

Up Arrow: Restart After Failure

## Can I Use This?

Absolutely!

As long as the license stays in the folder.

If that bothers you, then please, don't use this software.

## Dependencies

`pygame`

## How To Run

Run `main.py` with the command line option `-B` in `python3`.

## How To Have Fun

Just Enjoy!
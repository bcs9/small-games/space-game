import pygame

class sprite:
    def __init__(self, img, x, y, w, h):
        self.img = img
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.cx = 0
        self.cy = 0
    
    def draw(self, display):
        display.blit(pygame.transform.scale(self.img, (self.w, self.h)), (self.x, self.y))

    def move(self):
        self.x += self.cx
        self.y += self.cy
    
    def as_rect(self):
        return pygame.rect.Rect(self.x, self.y, self.w, self.h)
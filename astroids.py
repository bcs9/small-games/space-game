import pygame
import random
import sprite
import astroid

class astroids:
    def __init__(self, astroid_count):
        self.astroids = []

        for i in range(astroid_count):
            self.astroids.append(astroid.astroid(sprite.sprite(pygame.image.load("assets/astroid2.png"), random.randint(0, 1200), random.randint(-800, -100), 50, 50)))
            
    
    def move(self):
        for asteroidN in self.astroids:
            asteroidN.move()
    
    def draw(self, display):
        for astroidN in self.astroids:
            astroidN.draw(display)